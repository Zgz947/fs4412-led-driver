#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include "dev_info.h"

extern struct bus_type my_bus;

struct my_dev_desc led_dev_info[LED_Quantity]={ //  LED 信息
	[0]={
		.num =  1,
		.GP__CON = FS4412_GPX2CON,
		.GP__DAT = FS4412_GPX2DAT,
		.offset = 0x07,
	},
	[1]={
		.num =  2,
		.GP__CON = FS4412_GPX1CON,
		.GP__DAT = FS4412_GPX1DAT,
		.offset = 0x00,
	},
	[2]={
		.num =  3,
		.GP__CON = FS4412_GPF3CON,
		.GP__DAT = FS4412_GPF3DAT,
		.offset = 0x04,
	},
	[3]={
		.num =  4,
		.GP__CON = FS4412_GPF3CON,
		.GP__DAT = FS4412_GPF3DAT,
		.offset = 0x05,
	},
};

void my_dev_release(struct device *dev)
{
	printk("__-----%s-----__\n",__FUNCTION__);
}


//构建一个device对象
struct device my_dev= {
		.init_name = "my_dev_drv",
		.bus = &my_bus,
		.release = my_dev_release,
		.platform_data = &led_dev_info,
};



static int __init mydev_init(void)
{
	//将device 注册到总线中
	int ret,i;
	printk("__-----%s-----__\n",__FUNCTION__);
	
	ret = device_register(&my_dev);
	if(ret < 0)
	{
		printk("device_register error !\n");
		return ret;
	}
	return 0;
}

static void __exit mydev_exit(void)
{
	printk("__-----%s-----__\n",__FUNCTION__);
	//从总线中注销device
	device_unregister(&my_dev);
}



module_init(mydev_init);
module_exit(mydev_exit);
MODULE_LICENSE("GPL");

