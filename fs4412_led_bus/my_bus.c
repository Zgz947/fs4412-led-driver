#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>


int my_bus_match(struct device *dev, struct device_driver *drv)
{
	//如果匹配成功，match方法一定返回一个1，失败返回0
	if(!strncmp(drv->name,dev->kobj.name,strlen(drv->name)))
	{
		printk("match OK!\n");
		return 1;
	}
	else
	{
		printk("match failed !\n");
		return 0;
	}
}


//实例化一个bus对象
struct bus_type my_bus = {
			.name = "my_bus",
			.match = my_bus_match,
		
};

EXPORT_SYMBOL(my_bus);

static int __init mybus_init(void)
{	
	int ret;
	printk("______-----%s-----______\n",__FUNCTION__);	

	//构建一个总线
	//sys/bus/my_bus
	ret = bus_register(&my_bus);
	if(ret!=0)
	{
		printk("bus_register error !\n");
		return ret;
	}

	return 0;
}

static void __exit mybus_exit(void)
{
	printk("______-----%s-----______\n",__FUNCTION__);
	//注销一个总线
	bus_unregister(&my_bus);
}

module_init(mybus_init);
module_exit(mybus_exit);
MODULE_LICENSE("GPL");



