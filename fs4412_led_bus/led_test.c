#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include "dev_info.h"

int main(int argc, char **argv)
{
	int fd;
	unsigned long wait_time = 5555555;
	fd = open("/dev/led1", O_RDWR);
	if (fd < 0) {
		perror("open");
		exit(1);
	}
	if(argc  == 2)
	{
		int a = atoi(argv[1]);
		switch(a) {
			case 1: 
				ioctl(fd, LED_ON, &a);
				break;
			case 2: 
				ioctl(fd, LED_ON, &a);
				break;
			case 3: 
				ioctl(fd, LED_ON, &a);
				break;
			case 4: 
				ioctl(fd, LED_ON, &a);
				break;
			case 5:
				for(a=1;a<5;a++)
					ioctl(fd, LED_OFF, &a);
				wait_time=0;
				break;
			default :
				printf("input error!");
				return -1;
				break;
		}
		usleep(wait_time);
		ioctl(fd, LED_OFF, &a);

	}
	else
	{
		int i = 0;
		while(1)
		{	
			ioctl(fd, LED_ON, &i);
			usleep(200000);
			ioctl(fd, LED_OFF, &i);
			usleep(200000);
			if(++i == 5)
				i = 1;
		}
	}
	return 0;
}
