#ifndef __DEV_INFO_H__
#define __DEV_INFO_H__

#define LED_MAGIC 'L'
/*
 * need arg = 1/2 
 */
#define LED_Quantity 4

#define LED_ON	_IOW(LED_MAGIC, 0, int)
#define LED_OFF	_IOW(LED_MAGIC, 1, int)



#define FS4412_GPF3CON	0x114001E0
#define FS4412_GPF3DAT	0x114001E4

#define FS4412_GPX1CON	0x11000C20
#define FS4412_GPX1DAT	0x11000C24

#define FS4412_GPX2CON	0x11000C40
#define FS4412_GPX2DAT	0x11000C44


//设置一个自定义的数据，描述设备的特征
struct my_dev_desc{
		int num;
		unsigned volatile long GP__CON;
		unsigned volatile long GP__DAT;
		unsigned int *gp__con;
		unsigned int *gp__dat;
		int offset;
};



#endif
