#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>

#include "led_info.h"

#define GP__SIZE 24
#define GPF3CON	0x114001E0
#define GPF3DAT	0x114001E4

#define GPX1CON	0x11000C20
#define GPX1DAT	0x11000C24

#define GPX2CON	0x11000C40
#define GPX2DAT	0x11000C44


//设备 资源   信息
struct resource led_info[]={
	[0]={
			.start = GPX2CON,
			.end = GPX2DAT,
			.name = "0x07LED1",
			.flags = IORESOURCE_MEM,
	},
	[1]={
			.start = GPX1CON,
			.end = GPX1DAT,
			.name = "0x00LED2",
			.flags = IORESOURCE_MEM,
	},
	[2]={
			.start = GPF3CON,
			.end = GPF3DAT,
			.name = "0x04LED3",
			.flags = IORESOURCE_MEM,
	},
	[3]={
			.start = GPF3CON,
			.end = GPF3DAT,
			.name = "0x05LED4",
			.flags = IORESOURCE_MEM,
	},
};



struct platform_device led_dev={
		.name = "my_platform_led",		//用于平台总线设配匹配
		.id = -1,
		.num_resources = ARRAY_SIZE(led_info),
		.resource = led_info,
};


static int __init plat_led_dev_init(void)
{
	printk("---------------%s---------------\n",__FUNCTION__);
	//注册一个platform_device 
	return platform_device_register(&led_dev);
	
}

static void __exit plat_led_dev_exit(void)
{
	platform_device_unregister(&led_dev);//注销
	printk("---------------%s---------------\n",__FUNCTION__);
}


module_init(plat_led_dev_init);
module_exit(plat_led_dev_exit);
MODULE_LICENSE("GPL");


