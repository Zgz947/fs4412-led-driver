#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ioctl.h>

#include "fs4412_led.h"

int main(int argc, char **argv)
{
	int fd;
	fd = open("/dev/led1", O_RDWR);
	if (fd < 0) {
		perror("open");
		exit(1);
	}
	int i = 1,j=3;
	while(1)
	{	
		ioctl(fd, LED_ON, &i);
		ioctl(fd, LED_ON, &j);
		usleep(900000);
		ioctl(fd, LED_OFF, &i);
		ioctl(fd, LED_OFF, &j);
		usleep(900000); 
		i = i+1;
		j = j+1;
		if(i>2&&j>4)
	    {
			i = 1;
			j = 3;
		}	
	}
	
	return 0;
}
